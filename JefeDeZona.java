/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaventaproducto;

import java.util.ArrayList;

/**
 *
 * @author DAAD
 */
public class JefeDeZona  extends Empleado {
    

   String coche;
   ArrayList<Vendedor> vendedores = new ArrayList<>();
   ArrayList<Secretario> secretarios = new ArrayList<>();
 
public JefeDeZona(){
   super();
}    
public JefeDeZona(String nom, String ced, String dir, String tel, int ant,double sal,Empleado sup, String coche){
       super(nom, ced, dir, tel, ant, sal, sup);
       this.coche = coche;
}


public void asignarSecretario(Secretario nuevoSecretario){
       this.secretarios.add(nuevoSecretario);
       nuevoSecretario.cambiarSuperior(this);
}

public void mostrarSecretario(){
   for (int i=0; i< this.secretarios.size(); i++)
   {
       this.secretarios.get(i).mostrarDatos();
   }    
}

   public void mostrarDatos(){
   System.out.println("");
   super.mostrarDatos();
   System.out.println("El auto asignado es: "+this.coche);
   }

}