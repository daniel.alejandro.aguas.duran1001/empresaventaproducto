/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaventaproducto;

import java.util.ArrayList;

/**
 *
 * @author DAAD
 */
   public class Vendedor extends Empleado{
   String coche;
   String matricula;
   String marca;
   String modelo;
   int celular;
   String areaventa;
   double comision;
   ArrayList<Cliente> clientes = new ArrayList<>();
   
   
public Vendedor (String nom, String ced, String dir, String tel, int ant,double sal,Empleado sup,
       String coche, String matricula, String marca, String modelo, int celular, String areaventa, double comision){
       
       super(nom, ced, dir, tel, ant, sal, sup);
       this.coche = coche;
       this.matricula=matricula;
       this.marca=marca;
       this.modelo=modelo;
       this.celular=celular;
       this.areaventa=areaventa;
       this.comision=comision;
   }
   @Override

   public void mostrarDatos(){
   System.out.println("");
   super.mostrarDatos();
   System.out.println(this.coche+": "+this.marca+" "+this.modelo+", matricula: "+
   this.matricula);
   System.out.println("El celular es: "+celular+" su area es: "+areaventa+
   " y su comisión es: "+comision+ "% de las ventas.");
}

public void altaCliente(Cliente nuevoCliente){
       this.clientes.add(nuevoCliente);
       
}

public void bajaCliente(){
   for (int i=0; i< this.clientes.size(); i++)
   {
       this.clientes.get(i).mostrarDatos();
   }
}

   @Override
   public void cambiarAuto(String nuevoAuto){
   System.out.println("El nuevo auto asignado de "+this.nombre+
   " es: "+nuevoAuto);
   }

}
    

