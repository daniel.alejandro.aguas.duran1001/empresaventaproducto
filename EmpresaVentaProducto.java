/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaventaproducto;

/**
 *
 * @author DAAD
 */
public class EmpresaVentaProducto {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
JefeDeZona JefeZonaNorte, JefeZonaSur;
Vendedor Vendedor1, Vendedor2;
Secretario Secretario1, Secretario2;

        JefeDeZona JefeDeZonaNorte = new JefeDeZona ("Juan Perez","172254654","Valle los Chillos","0999985205",10,5000,null,"Vitara");
        JefeDeZona JefeDeZonaSur = new JefeDeZona ("Alejandro Reyes","1722781344","Valle los Chillos","0999985205",10,5000,null,"Hyunday");
Vendedor1 = new Vendedor ("Francsico Aguas","1722651344","Valle los Chillos","0999985205",10,5000,null,"Automovil","PSH 0211", "Chevrolet", "Aveo",965644444, "Ventas-Zona Valle", 5.0);
Vendedor2 = new Vendedor ("Jose Aguas","1722651344","Norte UIO","0994285665",10,5000,null,"SUV","PDH 4789", "Jeep", "Compass",954842369, "Ventas-Zona Norte", 5.0);
Secretario1 = new Secretario ("Estela Durán","1722651344","Norte UIO","0994285665",10,5000,null,52,"Propio");
Secretario2 = new Secretario ("Gabriela Rosero","1745123645","Sur UIO","0994285665",8,2000,null,12,"Alquilado");

System.out.println("\n"+"-LISTADO DE JEFES DE ZONA-");
JefeZonaNorte.mostrarDatos();
JefeZonaSur.mostrarDatos();
System.out.println("\n"+"-LISTADO DE SECRETARIOS-");
Secretario1.mostrarDatos();
Secretario2.mostrarDatos();
System.out.println("\n"+"-LISTADO DE VENDEDORES-");
Vendedor1.mostrarDatos();
Vendedor2.mostrarDatos();
System.out.println("\n"+"-SE ASIGNAN JEFES A LOS SECRETARIOS-");
JefeZonaNorte.asignarSecretario(Secretario2);
JefeZonaSur.asignarSecretario(Secretario1);
JefeZonaNorte.mostrarSecretario();
JefeZonaSur.mostrarSecretario();
System.out.println("\n"+"============================="+"\n");
System.out.println("\n"+"-SE AUMENTAN LOS SUELDOS A TODOS-");
System.out.println("\n"+"-A LOS VENDEDORES SOLO UN 10,0% ANUAL-");
Vendedor1.incrementarSalario(0.10);
Vendedor2.incrementarSalario(0.10);
System.out.println("\n"+"-A LOS SECRETARIOS SOLO 5,0% ANUAL-");
Secretario1.incrementarSalario(0.05);
Secretario2.incrementarSalario(0.05);
System.out.println("\n"+"-A LOS SECRETARIOS 20,0% ANUAL-");
JefeZonaNorte.incrementarSalario(0.20);
JefeZonaSur.incrementarSalario(0.20);
System.out.println("\n"+"============================="+"\n");
System.out.println("SE LES ASIGNA NUEVO AUTO A LOS JEFES"+"\n");
JefeZonaNorte.cambiarAuto("Citroen C5 Experience, Matricula AA835");
JefeZonaSur.cambiarAuto("Peugeot 508 XL, Matricula AA125"+"\n");
}

}

    
    

