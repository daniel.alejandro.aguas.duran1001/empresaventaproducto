/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaventaproducto;


/**
 *
 * @author DAAD
 */
public class Empleado {
   String nombre;
   private String dni;
   private String direccion;
   private String telefono;
   private int antiguedad;
   private double salario;
   private Empleado superior;
   
   public Empleado(){
   
   nombre= "Nombre y Apellido";
   dni= "Cédula";
   direccion= "Dirección";
   telefono= "Teléfono";
   antiguedad= 0;
   salario= 0;
   superior = this;
   }
   
   public Empleado(String nom, String ced, String dir, String tel, int ant,double sal,Empleado sup){

   this.nombre=nom;
   this.dni=ced;
   this.direccion=dir;
   this.telefono=tel;
   this.antiguedad=ant;
   this.salario=sal;
   this.superior=sup;
}
   public void mostrarDatos(){
   System.out.println("");
   System.out.println("Nombre: " + this.nombre);
   System.out.println("Cédula: " + this.dni);
   System.out.println("Dirección: " + this.direccion);
   System.out.println("Teléfono: " + this.telefono);
   System.out.println("Antiguedad: " + this.antiguedad);
   System.out.println("Salario: $" + this.salario);

   if(this.superior == null)
       {
       System.out.println("Superior: " + "no tiene superior");
       }
   else
       {
       System.out.println("Superior: " + this.superior.nombre );
       }
}

public void cambiarSuperior(Empleado nuevoSuperior){
   this.superior = nuevoSuperior;
   }

public void cambiarCliente(Empleado nuevoCliente){
   this.superior = nuevoCliente;
   }
public void cambiarAuto(String nuevoAuto){
   System.out.println("El nuevo auto asignado de "+this.nombre+
   " es: "+nuevoAuto);
   }

public void incrementarSalario(Double incrementoSalarial){
   System.out.println("");
   System.out.println("El sueldo previo al aumento: $"+this.salario);
   this.salario = this.salario + incrementoSalarial*this.salario;
   System.out.println("El sueldo con el aumento de: "+this.nombre+
   " es: $ "+this.salario);
   }
}




